import fs from 'fs';
import path from 'path';
import http from 'http';

const port = 8080;

http.createServer( (req, res) => {
    console.log('request starting...');

    let filePath = '.' + req.url;
    if (filePath === './')
        filePath = './index.html';

    const extname = path.extname(filePath);
    let contentType = 'text/html';
    if(extname === '.js') {
        contentType = 'text/javascript';
    }

    fs.readFile(filePath, (err, content) => {
        if (err) {
            if(err.code === 'ENOENT'){
                fs.readFile('./404.html', (error, content) => {
                    res.writeHead(200, { 'Content-Type': contentType });
                    res.end(content, 'utf-8');
                });
            }
            else {
                res.writeHead(500);
                res.end(`Sorry, server is down. Error: ${err.code}`);
                res.end();
            }
        }
        else {
            res.writeHead(200, { 'Content-Type': contentType });
            res.end(content, 'utf-8');
        }
    });

}).listen(port);
console.log(`Server running at localhost:${port}`);