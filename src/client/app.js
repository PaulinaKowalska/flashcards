import FlashcardStore from './utils/FlashcardStore';
import RequestService from './utils/RequestService';

require('./scss/style.scss');
import {answerA, answerB, restart} from './utils/selectorsDOM';
import {fillFlashcard} from './functions/fillFlashcard';
import {handleAnswerClick} from './functions/handleAnswerClick';
import {restartApp} from './functions/restartApp';

const url = 'https://gist.githubusercontent.com/vergilius/6d869a7448e405cb52d782120b77b82c/raw/e75dc7c19b918a9f0f5684595899dba2e5ad4f43/history-flashcards.json';

async function handleFlashcardApp() {
    await fillFlashcard(FlashcardStore.getFlashcard(0));
    answerA.addEventListener('click', handleAnswerClick);
    answerB.addEventListener('click', handleAnswerClick);
    restart.addEventListener('click', restartApp);
}

async function startFlashcardsApp() {
    //load data from url
    let data = await RequestService.getRequest(url);
    await FlashcardStore.setStore(data);
    await handleFlashcardApp();
}

startFlashcardsApp();