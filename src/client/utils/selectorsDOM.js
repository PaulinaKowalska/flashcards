const answerA = document.getElementById('answerA');
const answerB = document.getElementById('answerB');
const flashcard = document.getElementById('flashcard');
const pageBlockContent = document.getElementById('pageBlockContent');
const question = document.getElementById('question');
const restart = document.getElementById('restart');
const summary = document.getElementById('summary');
const flashcardShake = document.getElementById("flashcard-shake");

export {
    answerA,
    answerB,
    flashcard,
    flashcardShake,
    pageBlockContent,
    question,
    restart,
    summary
};