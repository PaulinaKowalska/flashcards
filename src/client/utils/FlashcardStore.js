class FlashcardStore {
    constructor() {
        this.store = [];
        this.storeCopy = [];
    }
    setStore(store) {
        this.store = store;
        this.storeCopy = new Array(...store);
    }
    resetStore() {
        this.store = new Array(...this.storeCopy);
    }
    removeFlashcard() {
        this.store.shift();
    }
    moveFlashcard() {
        this.store.push(this.store.shift());
    }
    getFlashcard(index) {
        return this.store[index];
    }
    isEmpty() {
        return this.store.length === 0;
    }
}
export default new FlashcardStore();