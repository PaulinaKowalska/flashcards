import {errorHandler} from './../functions/errorHandler';

class RequestService {

// async function
    async getRequest(url) {
        return await (await (fetch(url)
                .then(res => {
                    return res.json();
                })
                .catch(err => {
                    errorHandler();
                    console.log(`Error: ${err}`);
                })
        ));
    }
}

export default new RequestService();