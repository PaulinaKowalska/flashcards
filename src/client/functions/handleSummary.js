import { summary, flashcard } from './../utils/selectorsDOM';

export function handleSummary() {
    //show summary and hide flashcard container
    flashcard.style.display = 'none';
    summary.style.display = 'flex';
}