import { question, answerA, answerB } from './../utils/selectorsDOM';

export function fillFlashcard(flashcardData) {
    question.innerHTML = flashcardData.question;
    answerA.innerHTML = flashcardData.answers[0].answer;
    answerB.innerHTML = flashcardData.answers[1].answer;
    answerA.setAttribute('data', flashcardData.answers[0].correct);
    answerB.setAttribute('data', flashcardData.answers[1].correct);
}