import FlashcardStore from './../utils/FlashcardStore';
import {flashcard, summary} from './../utils/selectorsDOM';
import {fillFlashcard} from './fillFlashcard';

export function restartApp() {
    //show flashcard and hide summary container
    flashcard.style.display = 'block';
    summary.style.display = 'none';
    //set again initial flashcard store
    FlashcardStore.resetStore();
    fillFlashcard(FlashcardStore.getFlashcard(0));
}