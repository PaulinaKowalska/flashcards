import {pageBlockContent} from './../utils/selectorsDOM';

export function errorHandler() {
    pageBlockContent.innerHTML =
        `<div class="error-box sg-box sg-box--with-shadow">
            <div class="sg-box__hole">
                <h2 class="error-box-text sg-text-bit sg-text-bit--warning sg-text-bit--not-responsive">
                    !error
                </h2>
            </div>       
        </div>`;
}