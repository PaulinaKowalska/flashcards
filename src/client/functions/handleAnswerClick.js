import FlashcardStore from './../utils/FlashcardStore';
import {fillFlashcard} from './fillFlashcard';
import {handleSummary} from './handleSummary';
import {flashcardShake} from "../utils/selectorsDOM";

export function handleAnswerClick() {
    const attr = this.getAttribute('data');
     if(attr === 'true') {
         FlashcardStore.removeFlashcard();
         if (FlashcardStore.isEmpty()) {
             handleSummary();
         } else {
             fillFlashcard(FlashcardStore.getFlashcard(0));
         }
     } else {
         //shake effect
         flashcardShake.classList.remove("flashcard-box__shake");
         window.setTimeout(() => {
             flashcardShake.classList.add("flashcard-box__shake");
         }, 25);
         FlashcardStore.moveFlashcard();
         if (FlashcardStore.isEmpty()) {
             handleSummary();
         } else {
             fillFlashcard(FlashcardStore.getFlashcard(0));
         }
     }
}
