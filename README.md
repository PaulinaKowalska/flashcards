## Flashcards App

### Installation
1. Clone repository
2. Install dependencies - `yarn install`
3. Build project using webpack - `yarn run webpack`
4. Run watcher for server - `yarn run server-watch`
5. Website will be available on `localhost:8080`

Static website of this project is available on this link:
https://paulinakowalska.github.io/flashcards-app/
